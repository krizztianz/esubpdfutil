package com.kristianandi;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class ImageUtil {

	/**
	 * Convert given image file to PDF
	 * 
	 * @param imageFile
	 * @param outputFile
	 * @throws Exception
	 */
	public String createPDFFromImage(File imageFile, String outputFile) throws Exception {
		if(imageFile.exists()) {
			try (PDDocument doc = new PDDocument()) {
				PDImageXObject imgobject = PDImageXObject.createFromFileByContent(imageFile, doc);
				PDRectangle rect = getImageArea(imgobject);
				PDPage page = new PDPage(rect);
				doc.addPage(page);
				
				try (PDPageContentStream contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, true, true)) {
					contentStream.drawImage(imgobject, 0, 0);
					contentStream.close();
				}
				
				doc.save(outputFile);
				doc.close();
				
				File resultFile = new File(outputFile);
				
				if(!resultFile.exists()) {
					throw new Exception("Result file not found: " + outputFile);
				}
				
				return outputFile;
			}
		} else {
			throw new Exception("Image source file not found!");
		}
	}
	
	private PDRectangle getImageArea(PDImageXObject img) throws Exception {
		BufferedImage bfrimage = img.getImage();
        float width = bfrimage.getWidth();
        float height = bfrimage.getHeight();
        return new PDRectangle(width, height);
	}

}
