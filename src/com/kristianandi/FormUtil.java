package com.kristianandi;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FormUtil {

	public void createPDFFromJson(File pdfTemplate, File jsonfile, String outputFile) throws Exception {
		String content = new String(Files.readAllBytes(jsonfile.toPath()));

		if (!content.isEmpty()) {
			try (PDDocument pdfDocument = PDDocument.load(pdfTemplate)) {
				// get the document catalog
				PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

				// as there might not be an AcroForm entry a null check is necessary
				if (acroForm != null) {
					JSONParser parser = new JSONParser();
					List<JSONObject> jsonData = (List<JSONObject>) parser.parse(content);

					if (!jsonData.isEmpty()) {
						for (JSONObject item : jsonData) {
							String fieldName = (String) item.get("fieldName");
							String fieldType = (String) item.get("fieldType");
							Object value = item.get("value");
							Object pageNumber = item.get("pageNumber");
							if (fieldType.equalsIgnoreCase("textbox")) {
								PDTextField txtfield = (PDTextField) acroForm.getField(fieldName);
								if (txtfield != null) {
									txtfield.setValue(value != null ? value.toString() : "");
									txtfield.setReadOnly(true);
								}

							} else if (fieldType.equalsIgnoreCase("radio")) {
								PDRadioButton rdfield = (PDRadioButton) acroForm.getField(fieldName);
								if (rdfield != null) {
									rdfield.setValue(value != null ? value.toString() : "Off");
									rdfield.setReadOnly(true);
								}

							} else if (fieldType.equalsIgnoreCase("checkbox")) {
								PDCheckBox chkfield = (PDCheckBox) acroForm.getField(fieldName);
								if (chkfield != null) {
									chkfield.setValue(value != null ? value.toString() : "Off");
									chkfield.setReadOnly(true);
								}

							} else if (fieldType.equalsIgnoreCase("buttonImage")) {
								PDButton btnSignature = (PDButton) acroForm.getField(fieldName);
								if (btnSignature != null && value != null && pageNumber != null) {
									Long pg = (Long) pageNumber - 1;

									if (pg >= 0) {
										List<PDAnnotationWidget> widgets = btnSignature.getWidgets();
										PDAnnotationWidget annotationWidget = widgets.get(0);
										PDImageXObject pdImageXObject = PDImageXObject.createFromFile(value.toString(),
												pdfDocument);

										PDRectangle buttonPosition = getFieldArea(btnSignature);
										float height = buttonPosition.getHeight();
										float width = buttonPosition.getWidth();
										float x = buttonPosition.getLowerLeftX();
										float y = buttonPosition.getLowerLeftY();
										PDPage page = pdfDocument.getPage(pg.intValue());

										try (PDPageContentStream pdPageContentStream = new PDPageContentStream(
												pdfDocument, page, AppendMode.APPEND, true, true)) {
											pdPageContentStream.drawImage(pdImageXObject, x, y, width, height);
											pdPageContentStream.close();
											acroForm.getFields().remove(btnSignature);
										}
									} else {
										throw new Exception("Page number is less than zero");
									}

//		                            pdAppearanceStream.setBBox(new PDRectangle(x, y, width, height));
//
//		                            PDAppearanceDictionary pdAppearanceDictionary = annotationWidget.getAppearance();
//		                            if (pdAppearanceDictionary == null) {
//		                                pdAppearanceDictionary = new PDAppearanceDictionary();
//		                                annotationWidget.setAppearance(pdAppearanceDictionary);
//		                            }
//
//		                            pdAppearanceDictionary.setNormalAppearance(pdAppearanceStream);
								}
							}
						}

						acroForm.flatten();

						// Save and close the filled out form.
						pdfDocument.save(outputFile);
						pdfDocument.close();

						Thread.sleep(3500, 0);

						File outputResultFile = new File(outputFile);
						if (outputResultFile.exists()) {
							System.out.println("[Success] -> File saved to :" + outputFile);
						} else {
							throw new Exception("The output file not found!");
						}

					} else {
						throw new Exception("The parsed JSON does not contain any data!");
					}
				} else {
					throw new Exception("The PDF file doesn't contain any form!");
				}
			}
		} else {
			throw new Exception("The JSON file is empty!");
		}
	}

	private PDRectangle getFieldArea(PDField field) {
		COSDictionary fieldDict = field.getCOSObject();
		COSArray fieldAreaArray = (COSArray) fieldDict.getDictionaryObject(COSName.RECT);
		return new PDRectangle(fieldAreaArray);
	}

}
