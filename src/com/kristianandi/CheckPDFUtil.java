package com.kristianandi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

public class CheckPDFUtil {

	public static String BeginCheck(File pdfTemplate) throws Exception {

		if (!pdfTemplate.exists()) {
			throw new Exception("PDF file not found!");
		}

		List<String> result = new ArrayList<String>();

		try (PDDocument pdfDocument = PDDocument.load(pdfTemplate)) {
			// get the document catalog
			PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

			if (acroForm != null) {
				List<PDField> fields = acroForm.getFields();

				if (fields.size() > 0) {
					for (PDField item : fields) {
						result.add(item.getFullyQualifiedName());
					}
				}
			}
		}

		String finalResult = StringUtils.joinWith("\r\n", result.toArray());
		return finalResult;
	}
}
