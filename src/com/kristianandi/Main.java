package com.kristianandi;

import java.io.File;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		try {
			if (args.length > 0) {
				String mode = args[0];

				if (mode.equalsIgnoreCase("image")) {
					if (args.length != 3) {
						usage();
					} else {

						String img1 = args[1];
						String outputPdf = args[2];

						if (outputPdf.isEmpty() || img1.isEmpty()) {
							usage();
						} else {

							File img1File = new File(img1);

							if (img1File.exists()) {
								ImageUtil imgUtil = new ImageUtil();
								String outputfile = imgUtil.createPDFFromImage(img1File, outputPdf);

								System.out.println(outputfile);
							} else {
								NotFound(img1);
							}
						}
					}
				} else if (mode.equalsIgnoreCase("form")) {
					String inputPdf = args[1];
					String inputjson = args[2];
					String outputPdf = args[3];

					if (inputPdf.isEmpty() || outputPdf.isEmpty() || inputjson.isEmpty()) {
						usage();
					} else {
						File inputPdfFile = new File(inputPdf);
						File inpurtJsonFile = new File(inputjson);

						if (inpurtJsonFile.exists()) {
							if (!inpurtJsonFile.exists()) {
								NotFound("JSON data file");
								return;
							}

							FormUtil formUtil = new FormUtil();
							formUtil.createPDFFromJson(inputPdfFile, inpurtJsonFile, outputPdf);
						} else {
							NotFound(inputPdf);
						}
					}

				} else if (mode.equals("checkJava")) {
					String javaver = System.getProperty("java.version");
					System.out.println("Java Version: " + javaver);
				} else if (mode.equals("checkForm")) {
					String pdfSource = args[1];
					File pdfFile = new File(pdfSource);

					String fields = CheckPDFUtil.BeginCheck(pdfFile);
					System.out.println(fields);
				} else {
					usage();
				}
			} else {
				usage();
			}
		} catch (Exception e) {
//			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//			LocalDateTime now = LocalDateTime.now();
//			System.out.println(dtf.format(now));
			System.err.println("[Error] -> " + e.getMessage() + "\r\n");
			e.printStackTrace();
		}
	}

	public static void usage() {
		int year = Calendar.YEAR;

		String copyright = "Copyright (c) 2022 Kristian Andi";
		if (year > 2022) {
			copyright = "Copyright (c) 2022-" + year + " Kristian Andi";
		}

		System.err.println("Usage:");
		System.err.println("Image Mode (Convert image to PDF):");
		System.err.println("EsubPDFUtil.jar image <image-path> <output-Pdf-path>");
		System.err.println("");
		System.err.println("Form Mode (Fill PDF form):");
		System.err.println("EsubPDFUtil.jar form <input-Pdf-path> <Json-file-path> <output-Pdf-path>");
		System.err.println("");
		System.err.println("Get Form Fields from PDF:");
		System.err.println("EsubPDFUtil.jar checkForm <input-Pdf-path>");
		System.err.println("");
		System.err.println("Get JAVA Version from the system:");
		System.err.println("EsubPDFUtil.jar checkJava");
		System.err.println(copyright);
	}

	public static void NotFound(String filename) {
		System.err.println("[Error] -> The file " + filename + " not found!");
		System.err.println("Make sure you use full absolute path and not relative path!");
	}

}
